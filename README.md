# Autoreply

Модуль, который автоматически отвечает на заданные вопросы

Пример использования:

```js
var Autoreply = require("node-autoreply");

var autoreply = new Autoreply();
autoreply.init(function(err){
	if(err) throw new Error(err);
	console.log(autoreply.getAnswer("првиет"));
});
```