var fs = require("fs");

function autoreplyHandler(){
	
}

autoreplyHandler.prototype.init = function(answers_base_src, cb){
	var self = this;
	if(typeof answers_base_src === "function" && !cb){cb = answers_base_src; answers_base_src = __dirname + "/reg.txt"}
	fs.exists(answers_base_src, function(exists){
		if(!exists) return cb("file with answers base not found");
		fs.readFile(answers_base_src, 'utf8', function(err, str){
			if(err) return cb(err);
			self.initBaseWithStr(str, cb);
		});
	});
}

autoreplyHandler.prototype.initBaseWithStr = function(str, cb){
	var arr = str.split("\r\n\r\n");
	if((arr.length % 2) != 0) return cb("file records format error");
	this.base = [];
	for(var i = 0; i < arr.length; i+=2){
		var reg = arr[i];
		var answ = arr[i+1].split("\r\n");
		this.base.push({reg: reg, answ: answ});
	}
	cb();
}

autoreplyHandler.prototype.getAnswer = function(str){
	if(!this.base) throw new Error("answers base not init");
	for(var i = 0; i < this.base.length; i++){
		var rec = this.base[i];
		var answ = rec.answ;
		var regex = new RegExp(rec.reg);
		if(regex.test(str)){
			if(answ.length == 0) return "не знаю что тебе ответить...";
			if(answ.length == 1) return answ[0];
			return answ[Math.floor(Math.random()*answ.length)];
		}
	}
	return "...";
}

module.exports = autoreplyHandler;